$(document).ready(function () {
    $(".c-form").each(function () {
        const $that = $(this);
        $that.on('submit', (e) => {
            e.preventDefault();
            console.log("--------------------------------------------")
            const formData = {
                username: $("#username").val(),
                email: $("#email").val(),
                password: $("#password").val()
            };
            const url = $that.attr('data-url');
            console.log(url)
            $.ajax({
                type: "POST",
                data: formData,
                url: url,
                success: function(data) {
                    if (data.redirectUrl) {
                        console.log(data.redirectUrl)
                        window.location.href = data.redirectUrl;
                    }
                },
                error: function (error) {
                    console.log("===", error)
                    if (error.responseJSON.length) {
                        console.log("---",error.responseJSON)
                        const messages = error.responseJSON.map(errorData => errorData.message).join('</br>')
                        console.log("messages", messages)
                        $("#box").html(messages)
                    }
                }
            })
        });
    })
});
