const express = require('express');
const path = require('path');
const authRouter = require('./routes/auth-router');
const mongoose = require('mongoose');
const passport = require('passport');
const session = require('express-session');
const { initializingPassport } = require("./passport-config");

const app = express();

initializingPassport(passport);

mongoose.connect('mongodb://localhost/authentication').then(() => {
    console.log("Mongodb connected")
}).catch(() => {
    console.log("Could not  connect Mongodb")
})

app.set('view engine', 'ejs');
app.set('views',path.resolve('views'))
app.use(express.static(path.resolve('public')));
app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
}))

app.use(passport.initialize());
app.use(passport.session())

app.use('/auth', authRouter)

app.use('/', (req, res) => {
    res.render('home')
})

app.use((req,res) => {
    res.status(404).sendFile(path.resolve('public/404.html'))
})

app.listen(process.env.PORT || 4000);
