const { User } = require('../models/user');
const mongoose = require('mongoose');
const express = require('express');
const bcrypt = require('bcrypt');
const initializingPassport = require('../passport-config')
const Joi = require('joi');
const passport = require('passport')


function login(req,res) {
    return  res.render('login');
}

async function loginPost(req,res) {
    let user = await User.findOne({email: req.body.email});
    console.log(user)
    if (!user) return res.status(422).send([{message:'Invalid email'}])

    const validPassword = await bcrypt.compare(req.body.password, user.password)
    if (!validPassword) return res.status(422).send([{message:'Invalid  password'}])

     passport.authenticate('local', {
    successRedirect:"/",
    failureRedirect:"/auth/login"
     })
        return res.send({redirectUrl: '/home'})
}
function register(req,res) {
    return res.render('register')
}
async function registerPost(req,res) {

    const schema = Joi.object({
        username: Joi.string().min(3).max(200).required(),
        email: Joi.string().email().lowercase().required(),
        password: Joi.string().min(5).max(1024).required()
    })

    const { value, error } = schema.validate(req.body)
    console.log("value",value, "error",error)

    if (error){ return res.status(422).send(error.details) }
    let user = await User.findOne({email: req.body.email});
    if (user) return res.status(422).send([{message: 'User already registered'}])

    user = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
    })

        user.password = await bcrypt.hash(user.password, 10)


    await user.save();

    return res.send({redirectUrl: '/auth/login'});
}

module.exports = {
    login,
    loginPost,
    register,
    registerPost
}
