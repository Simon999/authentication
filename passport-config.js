const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const passport = require('passport');
const { User } = require('./models/user');
const bcrypt = require('bcrypt')

exports.initializingPassport = (passport) => {
    passport.use(
        new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
            // Match user
            User.findOne({
                email: email
            }).then(user => {
                console.log("user", user);
                if (!user) {
                    //return res.json(info);
                    return done(null, false, { message: 'That email is not registered' });
                }

                // Match password
                bcrypt.compare(password, user.password, (err, isMatch) => {
                    if (err) throw err;
                    if (isMatch) {
                        return done(null, user);
                    } else {
                        return done(null, false, { message: 'Password incorrect' });
                    }
                });
            });
        })
    );

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });
};

// exports.initializingPassport = (passport) => {
//     passport.use(
//         new LocalStrategy({ usernameField: "email"},async (email, password, done) => {
//             try {
//                 const user = await User.findOne((user) => user.email === email);
//                 console.log("user",user)
//                 if(user === undefined){
//                     return done(null, null, {message: "Incorrect email"})
//                 }
//                 if(await bcrypt.compare(password, user.password)){
//                     return done(null,user);
//                 }} catch (e) {
//                 return done(e, false)
//             }
//     }))
//
//     passport.serializeUser( async (id, done) => {
//         done(null, user.id)
//     })
//
//     passport.deserializeUser( async (id, done) => {
//         try {
//           const user = await User.findById(id);
//         }catch (e) {
//             done(error, false)
//         }
//     })
//
//
// }

