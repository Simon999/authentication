const express = require('express');
const router = express.Router();
const AuthController = require('../controllers/auth-controller');
const initializingPassport= require('../passport-config')
const passport = require('passport')

router.get('/login', AuthController.login);
router.post('/login', AuthController.loginPost);
router.get('/register', AuthController.register);
router.post('/register', AuthController.registerPost);

module.exports =  router;
